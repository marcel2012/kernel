#include <efi.h>
#include <efilib.h>
#include "elf.h"

int equals(void *ptr1, void *ptr2, int bytes) {
    char *a = ptr1, *b = ptr2;
    for (int i = 0; i < bytes; i++) {
        if (a[i] != b[i]) {
            return 0;
        }
    }
    return 1;
}

EFI_STATUS
//EFIAPI
efi_main(EFI_HANDLE image, EFI_SYSTEM_TABLE *systab) {
    InitializeLib(image, systab);
    Print(L"MK loader\n");

    EFI_STATUS status;

    // graphics mode

    VOID *graphics_buffer = PT_NULL;
    int graphics_width;
    int graphics_height;

    {
        EFI_GUID gopGuid = EFI_GRAPHICS_OUTPUT_PROTOCOL_GUID;
        EFI_GRAPHICS_OUTPUT_PROTOCOL *gop;

        status = uefi_call_wrapper(BS->LocateProtocol, 3, &gopGuid, NULL, (void **) &gop);
        if (EFI_ERROR(status)) {
            Print(L"ERROR LocateProtocol: %r\n", status);
            return EFI_LOAD_ERROR;
        }


        EFI_GRAPHICS_OUTPUT_MODE_INFORMATION *info;
        UINTN SizeOfInfo, numModes;

        status = uefi_call_wrapper(gop->QueryMode, 4, gop, gop->Mode == NULL ? 0 : gop->Mode->Mode, &SizeOfInfo, &info);
        if (EFI_ERROR(status)) {
            Print(L"ERROR QueryMode: %r\n", status);
            return EFI_LOAD_ERROR;
        }

        numModes = gop->Mode->MaxMode;
        for (int i = 0; i < numModes; i++) {
            status = uefi_call_wrapper(gop->QueryMode, 4, gop, i, &SizeOfInfo, &info);
            if (info->HorizontalResolution == 1920 && info->VerticalResolution == 1080) {
                status = uefi_call_wrapper(gop->SetMode, 2, gop, i);
                if (EFI_ERROR(status)) {
                    Print(L"ERROR SetMode: %r\n", status);
                    return EFI_LOAD_ERROR;
                }
            }
        }

        Print(L"video buffer 0x%lx\n", gop->Mode->FrameBufferBase);

        graphics_buffer = (void *) gop->Mode->FrameBufferBase;
        graphics_width = gop->Mode->Info->HorizontalResolution;
        graphics_height = gop->Mode->Info->VerticalResolution;

    }

    EFI_ALLOCATE_POOL AllocatePool = systab->BootServices->AllocatePool;
    EFI_GET_MEMORY_MAP GetMemoryMap = systab->BootServices->GetMemoryMap;

    EFI_FILE_PROTOCOL *kernel = NULL;

    // find kernel

    {
        EFI_GUID sfspGuid = EFI_SIMPLE_FILE_SYSTEM_PROTOCOL_GUID;
        EFI_HANDLE *handles = NULL;
        UINTN handleCount = 0;

        status = uefi_call_wrapper(systab->BootServices->LocateHandleBuffer, 5, ByProtocol,
                                   &sfspGuid,
                                   NULL,
                                   &handleCount,
                                   &handles);
        if (EFI_ERROR(status)) {
            Print(L"ERROR LocateHandleBuffer: %r\n", status);
            return EFI_LOAD_ERROR;
        }
        Print(L"FileSystems: %d\n", handleCount);

        for (int i = 0; i < handleCount; i++) {
            EFI_SIMPLE_FILE_SYSTEM_PROTOCOL *fs = NULL;

            status = uefi_call_wrapper(systab->BootServices->HandleProtocol, 3,
                                       handles[i],
                                       &sfspGuid,
                                       (void **) &fs);
            if (EFI_ERROR(status)) {
                Print(L"ERROR HandleProtocol: %r\n", status);
                return EFI_LOAD_ERROR;
            }

            EFI_FILE_PROTOCOL *root = NULL;

            status = uefi_call_wrapper(fs->OpenVolume, 2, fs, &root);
            if (EFI_ERROR(status)) {
                Print(L"ERROR OpenVolume: %r\n", status);
                return EFI_LOAD_ERROR;
            }

            EFI_FILE_PROTOCOL *token = NULL;

            status = uefi_call_wrapper(root->Open, 5,
                                       root,
                                       &token,
                                       L"kernel.elf",
                                       EFI_FILE_MODE_READ,
                                       EFI_FILE_READ_ONLY | EFI_FILE_HIDDEN | EFI_FILE_SYSTEM);
            if (EFI_ERROR(status)) {
                Print(L"ERROR Open: %r\n", status);
                continue;
            }

            kernel = token;
            break;
        }
    }

    if (kernel == NULL) {
        Print(L"ERROR kernel.efi not found\n");
        return EFI_LOAD_ERROR;
    }

    Print(L"kernel found\n");

    // read kernel elf header

    Elf64_Ehdr header;
    {
        UINTN FileInfoSize;
        EFI_FILE_INFO *FileInfo;
        status = uefi_call_wrapper(kernel->GetInfo, 4, kernel, &gEfiFileInfoGuid, &FileInfoSize, NULL);
        if (status != EFI_BUFFER_TOO_SMALL && status != EFI_SUCCESS) {
            Print(L"ERROR GetInfo 1: %r\n", status);
            return EFI_LOAD_ERROR;
        }

        status = uefi_call_wrapper(AllocatePool, 3, EfiLoaderData, FileInfoSize, (void **) &FileInfo);
        if (EFI_ERROR(status)) {
            Print(L"ERROR AllocatePool: %r\n", status);
            return EFI_LOAD_ERROR;
        }

        status = uefi_call_wrapper(kernel->GetInfo, 4, kernel, &gEfiFileInfoGuid, &FileInfoSize, (void **) &FileInfo);
        if (EFI_ERROR(status)) {
            Print(L"ERROR GetInfo 2: %r\n", status);
            return EFI_LOAD_ERROR;
        }

        UINTN size = sizeof(header);
        status = uefi_call_wrapper(kernel->Read, 3, kernel, &size, &header);
        if (EFI_ERROR(status)) {
            Print(L"ERROR Read: %r\n", status);
            return EFI_LOAD_ERROR;
        }
    }

    if (
            !equals(&header.e_ident[EI_MAG0], ELFMAG, 4) ||
            header.e_ident[EI_CLASS] != ELFCLASS64 ||
            header.e_ident[EI_DATA] != ELFDATA2LSB ||
            header.e_type != ET_EXEC ||
            header.e_machine != EM_AMD64 ||
            header.e_version != EV_CURRENT
            ) {
        Print(L"ERROR bad kernel elf format\n");
        return EFI_LOAD_ERROR;
    }

    // read kernel headers

    Elf64_Phdr *phdrs;
    {
        status = uefi_call_wrapper(kernel->SetPosition, 2, kernel, header.e_phoff);
        if (EFI_ERROR(status)) {
            Print(L"ERROR SetPosition: %r\n", status);
            return EFI_LOAD_ERROR;
        }

        UINTN size = header.e_phnum * header.e_phentsize;
        status = uefi_call_wrapper(AllocatePool, 3, EfiLoaderData, size, (void **) &phdrs);
        if (EFI_ERROR(status)) {
            Print(L"ERROR AllocatePool: %r\n", status);
            return EFI_LOAD_ERROR;
        }

        status = uefi_call_wrapper(kernel->Read, 3, kernel, &size, phdrs);
        if (EFI_ERROR(status)) {
            Print(L"ERROR Read: %r\n", status);
            return EFI_LOAD_ERROR;
        }
    }

    EFI_ALLOCATE_PAGES AllocatePages = systab->BootServices->AllocatePages;
    Elf64_Addr kernel_memory_segment = PT_NULL;

    // calculate kernel size

    UINTN max_addr = 0;

    for (
            Elf64_Phdr *phdr = phdrs;
            (char *) phdr < (char *) phdrs + header.e_phnum * header.e_phentsize;
            phdr = (Elf64_Phdr *) ((char *) phdr + header.e_phentsize)
            ) {
        if (phdr->p_vaddr > max_addr) {
            max_addr = phdr->p_vaddr + phdr->p_memsz;
        }
    }

    UINTN pages = max_addr / 4096 + 1;

    // allocate memory for kernel

    status = uefi_call_wrapper(AllocatePages, 4, AllocateAnyPages, EfiLoaderData, pages, &kernel_memory_segment);
    if (EFI_ERROR(status)) {
        Print(L"ERROR AllocatePages: %r\n", status);
        return EFI_LOAD_ERROR;
    }

    // load kernel to memory

    for (
            Elf64_Phdr *phdr = phdrs;
            (char *) phdr < (char *) phdrs + header.e_phnum * header.e_phentsize;
            phdr = (Elf64_Phdr *) ((char *) phdr + header.e_phentsize)
            ) {
        if (phdr->p_type == PT_LOAD) {

            Elf64_Addr segment = kernel_memory_segment + phdr->p_vaddr;
            Print(L"0x%lx\n", segment);

            status = uefi_call_wrapper(kernel->SetPosition, 2, kernel, phdr->p_offset);
            if (EFI_ERROR(status)) {
                Print(L"ERROR SetPosition: %r\n", status);
                return EFI_LOAD_ERROR;
            }

            UINTN size = phdr->p_filesz;
            status = uefi_call_wrapper(kernel->Read, 3, kernel, &size, (void *) segment);
            if (EFI_ERROR(status)) {
                Print(L"ERROR Read: %r\n", status);
                return EFI_LOAD_ERROR;
            }
        }
    }

    VOID *kernel_main = (void *) kernel_memory_segment + header.e_entry;

    Print(L"Get memory map and kernel jump\n");
    Print(L"header entry 0x%lx\n", header.e_entry);
    Print(L"jump address 0x%lx\n", kernel_main);

    EFI_MEMORY_DESCRIPTOR *Map = NULL;
    UINTN MapSize = 0, MapKey;
    UINTN DescriptorSize;
    UINT32 DescriptorVersion;
    {
        status = uefi_call_wrapper(GetMemoryMap, 5, &MapSize, Map, &MapKey, &DescriptorSize, &DescriptorVersion);
        if (status != EFI_BUFFER_TOO_SMALL) {
            Print(L"ERROR GetMemoryMap 1: %r\n", status);
            return EFI_LOAD_ERROR;
        }

        status = uefi_call_wrapper(AllocatePool, 3, EfiLoaderData, MapSize, (void **) &Map);
        if (EFI_ERROR(status)) {
            Print(L"ERROR AllocatePool: %r\n", status);
            return EFI_LOAD_ERROR;
        }

        status = uefi_call_wrapper(GetMemoryMap, 5, &MapSize, Map, &MapKey, &DescriptorSize, &DescriptorVersion);
        if (EFI_ERROR(status)) {
            Print(L"ERROR GetMemoryMap 2: %r\n", status);
            return EFI_LOAD_ERROR;
        }
    }

    status = uefi_call_wrapper(systab->BootServices->ExitBootServices, 2, image, MapKey);
    if (EFI_ERROR(status)) {
        Print(L"ERROR ExitBootServices: %r\n", status);
        return EFI_LOAD_ERROR;
    }

    // change memory to virtual mode

    status = uefi_call_wrapper(systab->RuntimeServices->SetVirtualAddressMap, 4, MapSize, DescriptorSize,
                               DescriptorVersion, Map);
    if (EFI_ERROR(status)) {
        Print(L"ERROR SetVirtualAddressMap: %r\n", status);
        return EFI_LOAD_ERROR;
    }

    ((__attribute__((sysv_abi)) void (*)()) kernel_main)(graphics_buffer, graphics_width, graphics_height,
                                                         (void *) kernel_memory_segment);

    return EFI_SUCCESS;
}
