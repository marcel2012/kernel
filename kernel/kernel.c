#include "interrupts.h"
#include "keyboard.h"
#include "graphics.h"
#include "paging.h"

void kernel_main(void *g_b, int w, int h, uint64_t memory_start) {

    // gdb breakpoint
    // asm volatile ("1: jmp 1b");
    // (gdb) set $pc += 2

    setup_interrupts(memory_start);

    setup_graphics(g_b, w, h);

    enable_page_table_editing();

    while (1) {
        check_keyboard();
    }
}