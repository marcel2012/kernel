#include "interrupts.h"
#include "helpers.h"


struct interrupt_descriptor_table {
    uint16_t offset_1; // offset bits 0..15
    uint16_t selector; // a code segment selector in GDT or LDT
    uint8_t ist;       // bits 0..2 holds Interrupt Stack Table offset, rest of bits zero.
    uint8_t type_attr; // type and attributes
    uint16_t offset_2; // offset bits 16..31
    uint32_t offset_3; // offset bits 32..63
    uint32_t zero;     // reserved
} __attribute__((packed));

struct interrupt_descriptor_table_header {
    uint16_t size;
    struct interrupt_descriptor_table *table;
} __attribute__((packed)) IDT_header;

void load_idt(struct interrupt_descriptor_table_header *ptr);

void store_idt(struct interrupt_descriptor_table_header *ptr);

void keyboard_int();

void all_int();

char get_cs();

static inline void outb(uint16_t port, uint8_t val) {
    asm volatile ( "outb %0, %1" : : "a"(val), "Nd"(port));
}

void setup_interrupts(uint64_t memory_start) {
    store_idt(&IDT_header);
    struct interrupt_descriptor_table *idt_table = IDT_header.table;

    uint64_t keyboard_int_pointer = memory_start + (uint64_t) & keyboard_int;
    uint64_t all_int_pointer = memory_start + (uint64_t) & all_int;


    outb(0x20, 0x11);
    outb(0xA0, 0x11);
    outb(0x21, 0x20);
    outb(0xA1, 40);
    outb(0x21, 0x04);
    outb(0xA1, 0x02);
    outb(0x21, 0x01);
    outb(0xA1, 0x01);
    outb(0x21, 0x0);
    outb(0xA1, 0x0);

    char cs = get_cs();

    for (int index = 0; index <= IDT_header.size; index++) {
        uint64_t function_ptr = (index == 33 ? keyboard_int_pointer : all_int_pointer);

        idt_table[index].offset_1 = extract_sub_bits(function_ptr, 0, 15);
        idt_table[index].selector = cs; /* KERNEL_CODE_SEGMENT_OFFSET */
        idt_table[index].zero = 0;
        idt_table[index].type_attr = 0x8e; /* INTERRUPT_GATE */
        idt_table[index].offset_2 = extract_sub_bits(function_ptr, 16, 31);
        idt_table[index].offset_3 = extract_sub_bits(function_ptr, 32, 63);
    }

    load_idt(&IDT_header);
}