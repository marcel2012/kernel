#ifndef INTERRUPTS_H
#define INTERRUPTS_H

#include <stdint.h>

void setup_interrupts(uint64_t memory_start);

#endif