#ifndef KEYBOARD_H
#define KEYBOARD_H

#include <stdint.h>

void key_press_event(uint64_t keycode);

void check_keyboard();

#endif