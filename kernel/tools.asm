section .text
extern key_press_event

global get_cs
get_cs:
    xor rax, rax
    mov ax, cs
    ret

global all_int
all_int:
    mov al, 0x20
    out 0x20, al
    iretq

global keyboard_int
keyboard_int:
    push rax

    mov al, 0x20
    out 0x20, al

    xor rax, rax
    in al, 0x60
    mov rdi, rax
    call key_press_event

    pop rax
    iretq

global load_idt
load_idt:
    lidt [rdi]
    sti
    ret

global store_idt
store_idt:
    sidt [rdi]
    ret

global get_cr3
get_cr3:
    mov rax, cr3
    ret

global disable_write_protection
disable_write_protection:
    mov rax, cr0
    and rax, ~0x10000
    mov cr0, rax
    ret

global enable_write_protection
enable_write_protection:
    mov rax, cr0
    or rax, 0x10000
    mov cr0, rax
    ret