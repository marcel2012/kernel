#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <stdint.h>

void setup_graphics(void *g_b, int w, int h);

uint64_t get_graphics_middle_address();

void draw_x();

void draw_d();

void clear_screen();

#endif