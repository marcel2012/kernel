#include "keyboard.h"
#include "paging.h"
#include "graphics.h"

uint64_t pressed_keycode = 0;

void key_press_event(uint64_t keycode) {
    pressed_keycode = keycode;
}

void check_keyboard() {
    int keycode = pressed_keycode;
    pressed_keycode = 0;
    switch (keycode) {
        case 0x20: // d
            draw_d();
            break;
        case 0x2d: // x
            draw_x();
            break;
        case 0x2e: // c
            clear_screen();
            break;
        case 0x10: // q
            disable_write_permission(get_graphics_middle_address());
            break;
        case 0x11: // w
            enable_write_permission(get_graphics_middle_address());
            break;
    }
}