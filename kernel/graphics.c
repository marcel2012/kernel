#include "graphics.h"

void *graphics_buffer;
int width;
int height;

void setup_graphics(void *g_b, int w, int h) {
    graphics_buffer = g_b;
    width = w;
    height = h;
}

uint64_t get_graphics_middle_address() {
    return (uint64_t) graphics_buffer + width * height * 3;
}

int abs(int a) {
    return (a < 0 ? -a : a);
}

int square(int a) {
    return a * a;
}

static void draw(unsigned char *buffer, int x, int y, unsigned char r, unsigned char g, unsigned char b) {
    int index = (width * y + x) * 4;
    buffer[index] = r;
    buffer[index + 1] = g;
    buffer[index + 2] = b;
}

static void
draw_rectangle(unsigned char *buffer, int x, int y, int w, int h, unsigned char r, unsigned char g, unsigned char b) {
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            draw(buffer, x + j, y + i, r, g, b);
        }
    }
}

void draw_x() {
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            if (y > height / 4 && y < height - height / 4) {
                if (abs((x + 0) - y) < 10) {
                    draw(graphics_buffer, x, y, 255, 255, 255);
                } else if (abs((x + 0) + y - height) < 10) {
                    draw(graphics_buffer, x, y, 255, 255, 255);
                }
            }
        }
    }
}

void draw_d() {
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            int r_2 = square(y - height / 2) + square(x - width * 5 / 8);
            if (x > width * 5 / 8 && r_2 > square(height / 4 - 20 / 1.41) &&
                r_2 <= square(height / 4)) {
                draw(graphics_buffer, x, y, 255, 255, 255);
            } else {
                int rectangle_x = width * 5 / 8;
                int rectangle_y = height / 4;
                int rectangle_w = 20 / 1.41;
                int rectangle_h = height / 2;
                if (x >= rectangle_x && x <= rectangle_x + rectangle_w && y >= rectangle_y &&
                    y <= rectangle_y + rectangle_h) {
                    draw(graphics_buffer, x, y, 255, 255, 255);
                }
            }
        }
    }
}

void clear_screen() {
    draw_rectangle(graphics_buffer, 0, 0, width, height, 0, 0, 0);
}
