#include "helpers.h"

uint64_t extract_sub_bits(uint64_t address, int start, int end) {
    address <<= 63 - end;
    address >>= start + (63 - end);
    return address;
}