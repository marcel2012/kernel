#include "paging.h"
#include "helpers.h"
#include "graphics.h"

void disable_write_protection();

void enable_write_protection();

uint64_t get_cr3();

uint64_t get_page_table_entry_address(uint64_t address) {
    uint64_t cr3 = get_cr3();
    uint64_t *lv3_ptr = (uint64_t *) cr3;
    uint64_t lv3 = lv3_ptr[extract_sub_bits(address, 39, 47)];
    lv3 &= ~(0xfff);
    uint64_t *lv2_ptr = (uint64_t *) lv3;
    uint64_t lv2 = lv2_ptr[extract_sub_bits(address, 30, 38)];
    lv2 &= ~(0xfff);
    uint64_t *lv1_ptr = (uint64_t *) lv2;
    int i = extract_sub_bits(address, 21, 29);
    return (uint64_t)((uint64_t *) lv1_ptr + i);
}

void change_write_permission(uint64_t address, uint8_t on) {
    uint64_t *page_table_entry_address = (uint64_t *) get_page_table_entry_address(address);

    if (on) {
        *page_table_entry_address |= 0b10;
    } else {
        *page_table_entry_address &= ~0b10;
    }
}

void enable_write_permission(uint64_t address) {
    change_write_permission(address, 1);
}

void disable_write_permission(uint64_t address) {
    change_write_permission(address, 0);
}

void enable_page_table_editing() {
    disable_write_protection();
    enable_write_permission(get_page_table_entry_address(get_graphics_middle_address()));
    enable_write_protection();
}