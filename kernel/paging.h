#ifndef PAGING_H
#define PAGING_H

#include <stdint.h>

void enable_write_permission(uint64_t address);

void disable_write_permission(uint64_t address);

void enable_page_table_editing();

#endif