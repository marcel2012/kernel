#ifndef HELPERS_H
#define HELPERS_H

#include <stdint.h>

uint64_t extract_sub_bits(uint64_t address, int start, int end);

#endif