### Artifacts
[Download kernel binary files](https://gitlab.com/marcel2012/kernel/-/jobs/artifacts/master/raw/kernel.zip?job=package)

UEFI app steps:
+ change screen resolution to FullHD
+ search all partitions for `kernel.elf` file
+ read kernel file headers
+ copy kernel segments to memory
+ change memory mode to virtual
+ jump to kernel

Kernel steps:
+ load own interrupts including keyboard support
+ save graphics informations
+ enable Page Table Entry editing
+ wait for events

Kernel functions:
+ press `c` to clear screen
+ press `x` to draw `x` letter
+ press `d` to draw `d` letter
+ press `w` to allow writing to bottom half of graphics buffer
+ press `q` to disallow writing to bottom half of graphics buffer

You can press `q` and try draw letter - processor should interrupt this